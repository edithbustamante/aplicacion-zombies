class AddRottenToZombie2s < ActiveRecord::Migration[5.0]
  def change
    add_column :zombie2s, :rotten, :boolean
  end
end
